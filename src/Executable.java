import java.util.InputMismatchException;
import java.util.Scanner;
// java -cp bin --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls Executable
public class Executable {
    public static void main(String[] args) {
        Grille grille = new Grille(5, 5);
        System.out.println(grille.getNbBombes());
        // System.out.println("Nbr Case Voisine" + grille.getNbBombesVoisine(ligneRepere, colonneRepere));
        Scanner scanMode = new Scanner(System.in);
        System.out.println("Séléctionner votre choix: \n 1 - Jouer avec une interface non graphique. \n 2 - Jouer avec une interface graphique.");
        int selection = scanMode.nextInt();
        if(selection == 2 ){
            DemineurGraphique.main(args);
        }
        else{
            while(!grille.estPerdue() && !grille.estGagnee()){
                grille.affiche();
                try{
                    Scanner scanChoix = new Scanner(System.in);
                    System.out.println("Nombres de Bombes: " + grille.getNbBombes());
                    System.out.println("Nombres de cases marquées: " + grille.getNombreDeCasesMarquees());
                    System.out.println("Nombres de cases découverte: " + grille.getNombreDeCasesRevelees());
                    System.out.println("Séléctionner votre choix: \n 1 - Marquer une case \n 2 - Decouvrir une case");
                    int choix = scanChoix.nextInt();
                
                    if (choix == 1){
                        System.out.println("Donnez le numéro de la ligne de la case à marqué ");
                        int ligne = scanChoix.nextInt();

                        System.out.println("Donnez le numéro de la colonne de la case à marqué ");
                        int colonne = scanChoix.nextInt();

                        //Gère si les coordonnées sont hors du jeu
                        try{
                            if(! grille.getCase(ligne, colonne).estRevelee()){
                                if(grille.getCase(ligne, colonne).estMarquee()){
                                    grille.getCase(ligne, colonne).deMarquer();
                                }
                                else{
                                grille.getCase(ligne, colonne).marquer();
                                }
                            }
                            else{
                                System.out.println("La case est découverte, elle ne peut pas être marqué.");
                            }
                            
                        }

                        catch (NullPointerException e){
                            System.out.println("Cette case n'existe pas");
                        }


                        // if (!grille.estGagnee() && !grille.estPerdue()){
                        // System.out.println(grille.getCase(ligne, colonne).estBombe());
                        //     }
                    }

                    else if (choix == 2){
                        System.out.println("Donnez le numéro de la ligne de la case à découvrir ");
                        int ligne = scanChoix.nextInt();

                        System.out.println("Donnez le numéro de la colonne de la case à découvrir ");
                        int colonne = scanChoix.nextInt();

                        //Gère si les coordonnées sont hors du jeu
                        try{
                        if(! grille.getCase(ligne, colonne).estMarquee()){
                            grille.getCase(ligne, colonne).reveler();
                        }
                        grille.getCase(ligne, colonne).deMarquer();
                        }

                        catch (NullPointerException e){
                            System.out.println("Cette case n'existe pas");
                        }

                        // // debugger
                        // if (!grille.estGagnee() && !grille.estPerdue()){
                        // System.out.println(grille.getCase(ligne, colonne).estBombe());
                        //     }
                    }

                    else{
                        System.out.println("ce nombre n'est pas une option");
                    }

                    //debugger
                }

                catch (InputMismatchException e){
                    System.out.println("Veuillez rentrer un chiffre");
                }
                

            }
            grille.affiche();
            if (grille.estGagnee()){
                System.out.println("Gagné!");
            }
            else{
                System.out.println("Perdue!");
                Scanner scanChoix = new Scanner(System.in);
                System.out.println("Séléctionner votre choix: \n 1 - Rejouer \n 2 - Quitter");
                int choix = scanChoix.nextInt();
                if(choix == 1){
                    main(args);
                }

            }
        }
    }
}
