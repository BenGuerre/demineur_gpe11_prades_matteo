import java.util.ArrayList;
import java.util.List;

public class Case{
    private boolean bombe;
    private boolean marquee;
    private boolean revelee;
    private List<Case> casesVoisines;

    public Case(){
        this.bombe = false;
        this.marquee = false;
        this.revelee = false;
        this.casesVoisines = new ArrayList<Case>();
    }

    /** setteur de this.bombe
     */ 
    public void ajouteBombe(){
        
        this.bombe = true;
    }

    /**
    getteur de this.bombe
    */
    public boolean estBombe(){
        return this.bombe;
    }
    
    /**getteur de this.revelee
    */
    public boolean estRevelee(){
        
        
        return this.revelee;
    }

    /**getteur de this.marquee
    */
    public boolean estMarquee(){
        
        
        return this.marquee;
    }
    
    /**getteur de this.revelee
    */ 
    public void reveler(){
        this.revelee = true;
    }

    /**getteur de this.bombe
    */
    public void marquer(){        
        this.marquee = true;
    }

    /**
     * Permet de démarquer une case
     */
    public void deMarquer(){
        this.marquee = false;
    }
    
    /**
    * getteur de this.casesVoisines
    * @return List<Case>
    */
    public List<Case> getCasesVoisines(){
        return this.casesVoisines;
    }

    /**
    * setteur de this.casesVoisines
    * @return void
    */
    public void ajouteCaseVoisine(Case Case){
        this.casesVoisines.add(Case);
    }

    /**
    * Retourne le nombre de bombe autour de cette case
    * @return int
    */
    public int getNbBombesVoisines(){
        int res = 0;
        for (Case caseEnCour : this.getCasesVoisines()){
            if (caseEnCour.estBombe()){
                res++;
            }
        }
        return res;
    }

    /**
    * Retourne un affichage différent selon l'état de la case
    * @return String
    */
    public String getAffichage(){  
        if (this.marquee) return "?";
        if (this.revelee && this.bombe) return "@";
        if (this.revelee) return String.valueOf(getNbBombesVoisines());
        return " ";
    }

    @Override
    public String toString() {
        return "["+ this.bombe + "," + this.marquee + "," + this.revelee + "," + this.casesVoisines + "]";
    }

}
