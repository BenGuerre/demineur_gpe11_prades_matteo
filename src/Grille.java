import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Grille{
    /** le nombre de lignes */
    private int nbLignes;
    /**Le nombre de colonnes */
    private int nbColonnes;
    /** Le nombre de bombes */
    private int nbBombes;
    /** Le dictionnaire avec les cases */
    private Map<Coordonnee, Case> mapCase;



    public Grille(int nbLignes, int nbColonnes){
        Random random = new Random();
        this.nbLignes = nbLignes;
        this.nbColonnes = nbColonnes;
        this.mapCase = new HashMap<>();
        this.nbBombes = 9;
        int bombePose = 0;
        // intitialisation des clée du dico
        for(int x=0 ; x<(nbLignes); x ++){
            for(int y=0 ; y<(nbColonnes); y ++){
                this.mapCase.put(new Coordonnee(x, y), new Case());
            }
        }
        // On pose les mines
        while(bombePose < this.nbBombes) {
            int xb = random.nextInt(nbLignes);
            int yb = random.nextInt(nbColonnes);
            Case caseABombe = this.getCase(xb, yb);
            if(! caseABombe.estBombe()){
                caseABombe.ajouteBombe();
                bombePose ++;
            }
        }
        // On met à jour les cases voisines
        for(Coordonnee coord: this.mapCase.keySet()){
            // On prend la case 
            Case caseEnTraitement = this.mapCase.get(coord);
            //On regarde si elle n'est pas sur des bords et on rend ces voisines
            Coordonnee coordPotentielVoisine = new Coordonnee(coord.getX() - 1, coord.getY() -1);
            if( mapCase.containsKey(coordPotentielVoisine)){
                caseEnTraitement.ajouteCaseVoisine(mapCase.get(coordPotentielVoisine));
            }
            coordPotentielVoisine = new Coordonnee(coord.getX(), coord.getY() -1);
            if( mapCase.containsKey(coordPotentielVoisine)){
                caseEnTraitement.ajouteCaseVoisine(mapCase.get(coordPotentielVoisine));
            }
            coordPotentielVoisine = new Coordonnee(coord.getX() + 1, coord.getY() -1);
            if( mapCase.containsKey(coordPotentielVoisine)){
                caseEnTraitement.ajouteCaseVoisine(mapCase.get(coordPotentielVoisine));
            }
            coordPotentielVoisine = new Coordonnee(coord.getX() - 1, coord.getY());
            if( mapCase.containsKey(coordPotentielVoisine)){
                caseEnTraitement.ajouteCaseVoisine(mapCase.get(coordPotentielVoisine));
            }
            coordPotentielVoisine = new Coordonnee(coord.getX(), coord.getY());
            if( mapCase.containsKey(coordPotentielVoisine)){
                caseEnTraitement.ajouteCaseVoisine(mapCase.get(coordPotentielVoisine));
            }
            coordPotentielVoisine = new Coordonnee(coord.getX() + 1, coord.getY());
            if( mapCase.containsKey(coordPotentielVoisine)){
                caseEnTraitement.ajouteCaseVoisine(mapCase.get(coordPotentielVoisine));
            }
            coordPotentielVoisine = new Coordonnee(coord.getX() - 1, coord.getY() + 1);
            if( mapCase.containsKey(coordPotentielVoisine)){
                caseEnTraitement.ajouteCaseVoisine(mapCase.get(coordPotentielVoisine));
            }
            coordPotentielVoisine = new Coordonnee(coord.getX(), coord.getY() + 1);
            if( mapCase.containsKey(coordPotentielVoisine)){
                caseEnTraitement.ajouteCaseVoisine(mapCase.get(coordPotentielVoisine));
            }
            coordPotentielVoisine = new Coordonnee(coord.getX() + 1, coord.getY() + 1);
            if( mapCase.containsKey(coordPotentielVoisine)){
                caseEnTraitement.ajouteCaseVoisine(mapCase.get(coordPotentielVoisine));
            }
            
        }
    
        
    }
    
    

    /**
     * Permet obtenir le nombre de lignes de la grille
     * @return le nombre de lignes
     */
    public int getNbLignes() {
        return this.nbLignes;
    }

    /**
     * Permet d'obtenir le nombre de colonnes de la grille
     * @return le nombre de colonnes
     */
    public int getNbColonnes() {
        return this.nbColonnes;
    }


    /**
     * Permet d'obtenir le nombre de bombes de la grille
     * @return le nombre de bombes
     */

    public int getNbBombes() {
        return this.nbBombes;
    }


    /**
     * Donne la case au coordonnées x et y  
     * @param x Coordonnées horizontales    
     * @param y coordonées verticales
     * @return la case | null si la case n'est pas dedans
     */
    public Case getCase(int x, int y){
        Coordonnee coord = new Coordonnee(x, y);
        for(Coordonnee c: this.mapCase.keySet()){
            // Sinon les coordonnées même identique ne pointes pas au même endroit en mémoire 
            if(c.equals(coord)){ 
                return this.mapCase.get(c);
            }
        }
        return null;
    }

    /**
     * Permet d'obtenir le nombre de cases révélées sur le plateau
     * @return nombre de cases révélée
     */
    public int getNombreDeCasesRevelees(){
        int res = 0;
        for( Case caseEnCour : this.mapCase.values()){
            if( caseEnCour.estRevelee()){ res ++;}
        }
        return res;
    }
    /**
     * Permet de savoire combien il y a de case marquée
     */
    public int getNombreDeCasesMarquees(){
        int res = 0;
        for( Case caseEnCour : this.mapCase.values()){
            if( caseEnCour.estMarquee()){res ++;}
        }
        return res;
    }


    public List<Case> getCasesVoisines(int x, int y){
        List<Case> casesVoisines = new ArrayList<Case>();
        Coordonnee coord = new Coordonnee(x,y);
        if(coord.getX() - 1 >= 0 && coord.getY() - 1 >= 0){
            Case casev = this.getCase(coord.getX() - 1,coord.getY() - 1);
            casesVoisines.add(casev);
        }
        if(coord.getX() - 1 >= 0 && coord.getY() + 1 < this.nbColonnes){
            Case casev = this.getCase(coord.getX() - 1, coord.getY() + 1);
            casesVoisines.add(casev);
        }
        if(coord.getX() + 1 >= this.nbLignes && coord.getY() - 1 >= 0){
            Case casev = this.getCase(coord.getX() + 1, coord.getY() - 1);
            casesVoisines.add(casev);
        }
        if(coord.getX() + 1 < this.nbLignes && coord.getY()+1 >= this.nbColonnes){
            Case casev = this.getCase(coord.getX() + 1, coord.getY() + 1);
            casesVoisines.add(casev);
        }
        if(coord.getX() - 1 >= 0){
            Case casev = this.getCase(coord.getX() - 1, coord.getY());
            casesVoisines.add(casev);
        }
        if(coord.getX() + 1 < this.nbLignes){
            Case casev = this.getCase(coord.getX() - 1, coord.getY());
            casesVoisines.add(casev);
        }
        if(coord.getY() - 1 >= 0){
            Case casev = this.getCase(coord.getX(), coord.getY() - 1);
            casesVoisines.add(casev);
        }
        if(coord.getY() + 1 < this.nbColonnes){
            Case casev = this.getCase(coord.getX(), coord.getY() + 1);
            casesVoisines.add(casev);
        }
    return casesVoisines;
    }

    public int getNbBombesVoisine(int x , int y){
        int res = 0;
        List<Case> casesVoisines = getCasesVoisines(x, y);
        for(Case caseVoisine: casesVoisines){
            if(caseVoisine != null){
                if(caseVoisine.estBombe()){
                    res ++;
                }
            }
        }
        // System.out.println(res);
        return res;

    }

    /**
     * Permet de savoir si la partie est perdue ou non
     */
    public boolean estPerdue(){
        for( Case caseEnCour : this.mapCase.values()){
            if( caseEnCour.estBombe() && caseEnCour.estRevelee()){
                return true;
            }
        }
        return false;
    }

    /**
     * Permet de savoir si la partie est terminée
     * @return si la partie est finie ou non
     */
    public boolean estGagnee(){
        for( Case caseEnCour : this.mapCase.values()){
            // si la case est cachée et non marquée
            if( !caseEnCour.estRevelee() && !caseEnCour.estMarquee()){
                return false;
            }
        }
        return true;
    }
    /**
     * Permet d'afficher la plateau sur un terminal
     */
    public void affiche(){
        // Affichage du plateau
        String plateau =  "";
        plateau += "    0";
        // initialisation du plateau 
        for(int i =1 ; i <nbColonnes; i ++){
            plateau += "   " + i;
        }
        plateau += "\n";
        String haut = "  ┌───┬";
        String milieu = "  ├───┼";
        String bas = "  └───┴";
        for (int i = 0; i<nbColonnes-2; i ++){
            haut += "───┬";
            bas += "───┴";
            milieu += "───┼";
        }
        haut += "───┐";
        bas += "───┘";
        milieu += "───┤";
        plateau += haut + "\n";

        for( int x=0 ; x<this.nbLignes; x++){
            String centreCase = x + " | ";
            for (int y=0 ; y<this.nbColonnes; y++){
                String marque = " ";
                Case caseEnCours = this.getCase(x, y);
                // Si la case est révélé
                if(caseEnCours.estRevelee()){
                    marque = String.valueOf(this.getNbBombesVoisine(x, y));
                // Si la case est une bombe
                    if (caseEnCours.estBombe()){
                        marque = "@";
                    }
                }
                // Si elle est marqué
                else if(caseEnCours.estMarquee()){
                    marque = "?";
                }
                centreCase += marque + " | ";
            }
            plateau += centreCase + "\n";
            if (x != nbColonnes-1){
                plateau += milieu + "\n";
            }   
        }
        plateau += bas;
        System.out.println(plateau);

            

    }
}
